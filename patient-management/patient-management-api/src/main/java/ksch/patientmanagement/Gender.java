package ksch.patientmanagement;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
